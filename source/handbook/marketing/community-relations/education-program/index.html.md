---
layout: handbook-page-toc
title: "Education Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

At GitLab, we believe that **every student can contribute**! The Education Program provides the top tiers of GitLab for free to students and faculty at educational institutions around the globe. We are invested in ensuring that students have access to the full functionality of GitLab while in school so they can become future contributors and evangelists of GitLab. 

The Education Program has exceeded our expectations on its own merit. As of January 2020 we reached over **740 educational institutions worldwide** and have **1.4 million users**. 

## Mission
The primary mission of the GitLab Education Program is to facilitate and drive the adoption of GitLab at educational institutions around the globe and build an engaged community of GitLab evangelists and contributors in the next generation of the workforce. 

Additionally, the Education Program seeks to evangelize the benefits of an all-remote operating model and GitLab's associated company values to the next generation of the workforce.

### Goals
The goals in building out the Education Program are: 
* Align the program structure and license offerings with the needs and operating models of educational institutions while providing the best GitLab has to offer to students, faculty, and staff. 
* Grow the base of educational institutions, students, faculty, and staff using GitLab.
* Build a robust and engaged educational community full of members who collaborate, contribute, and enable each other to be successful with GitLab. 
* Build meaningful relationships with the Education Program member institutions.
* Provide a wealth of resources for adopting GitLab in an educational setting including course materials, case studies, code examples, syllabi, and presentations. 
* Evangelize the benefits of DevOps as a discipline and GitLab as the leading single application for the DevOps lifecycle. 
* Bring DevOps into the classroom in related disciplines such as computer science and infrastructure technology. 
* Be a thought leader in the discipline of DevOps by engaging with related academic disciplines, academic organizations, and associations. 
* Evangelize the benefits of an all-remote operating model and GitLab's associated company values to the next generation of the workforce.

## Vision
The vision of the Education Program is to enable educational institutions to be successful in teaching, learning, conducting research with GitLab. We seek to build an engaged community of GitLab users around the world who actively contribute to Gitlab and each other’s success, and ultimately become evangelists of GitLab in the workplace and beyond.

## What we are working on
Education Program issues typically exist in the [Education Program subgroup](https://gitlab.com/gitlab-com/marketing/community-relations/education-program) of the [Community Relations Group](https://gitlab.com/gitlab-com/marketing/community-relations) but they can also exist in [Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing), [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing), or other [marketing subgroups](https://gitlab.com/gitlab-com/marketing).

We use the `education` label to track issues. The Education Program [issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/education-program/-/boards) provides an overview of these issues and thier status. Any [Epics](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics) that we are working on can be found in the Community Relations Group with the tag `education`.

### Education Program OKRs

#### [Q1 OKR: Ramp up engagement with existing program members.](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/8) 
1. Execute Education Program survey, with published results in a public blog post. 
2. Publish 3 use cases from existing member institutions, including thier logos on our website. 

#### [Q1 OKR: Increase Education Program awareness.](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/9) 
1. Event plan for 2021: execute on at least 1 event in Q1. 
2. Redesign Education Program landing page, with new terms, program definition, and institution member logos. 
3. Publish 5 blog posts on the Education Program. 

## Requirements

- Any institution whose purposes directly relates to learning, teaching, and/or training may qualify. Educational purposes do not include commercial, professional, or any other for-profit purposes. The education license is only for the purposes of educating students and can be used by students and the teachers providing the education.
- See our [full terms](/terms/#edu-oss) regarding this program
- For more examples, please see the [FAQ section](/solutions/education/#FAQ)

## Workflows

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)

## Metrics
The steps below will generate the Education Program metrics including: opportunity close date, tier, number of seats, billing city and billing country. These metrics are used in the Community Relations Group Conversation and for the Education Program Handbook. 

1. Log into SFDC. 
1. Navigate to Reports. 
1. Open the 'Education Opportunities Metrics' [SFDC Report](https://gitlab.my.salesforce.com/00O4M000004e4KE). 
1. Click `Run Report` to generate the report. 
1. Click `Export Details`. Choose `Unicode (UTF-8)` for the Export File Encoding and `.csv` for the Export File Format. Then click `Export` and save the file. 
1. Download the [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script. 
1. Open a command prompt and change to the directory where you have the script and .csv file. 
1. Run the Edoss script with the exported csv file as input to generate the final csv file. 
       `./edoss.rb -i <exported_file>.csv -o <output_file>.csv`
1. Update the [GitLab for Education Graphs](https://docs.google.com/spreadsheets/d/18zudgYDeL1Zy90mEIO_Xi8X-ZyE86N-eQ1Nluyrh1hY/edit#gid=1612197101) for the Community Relations Group Call by importing the results of the script and updating the figures. 
Notes: 
     - You may need to make the file executable by using the following command:
       `chmod 755 edoss.rb`
- Additionally, the processor script can provide the data in `yaml` and `json` formats. 


SFDC Opportunities by Stage for EDU campaign - SFDC [report](https://na34.salesforce.com/00O61000004hfnt)

## Resources for Education Program Participants
We are always looking for ways to better support our participants use of GitLab for teaching, learning, and research. Please reach out to us at education@GitLab.com with any ideas. 

### How to structure your projects
We often receive questions about how to best manage your licenses. Here are a few tips:
* GitLab does not use a named license model. This means that seats are generic and not specific to a user. If a user leaves your organization, you can remove or block that user to free the seat. This seat can then be used by another user.
* Although we offer only one license key for the self-hosted solutions, this key can be used on multiple independent instances. This means you can run multiple separate servers with the same license key. The only caveat is that currently there isn't an easy way to calculate the total number of consumed seats across all instances.
- You can manage the visibility of your projects with GitLab groups. A member of the parent group automatically has access to all descendants. GitLab doesn't support having the subgroup be more restrictive than its parent. However, being a part of a subgroup does not grant you access to the parent group. The best way to organize your work is to make everyone a member of their respective subgroup having only admins in the organizational (top-level) group. [Learn more about GitLab groups].(https://docs.gitlab.com/ee/user/group/).
- Please also see our [licensing and subscription FAQ](/pricing/licensing-faq/) section for more details.
