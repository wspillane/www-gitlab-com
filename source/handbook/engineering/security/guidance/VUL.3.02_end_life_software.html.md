---
layout: handbook-page-toc
title: "VUL.3.02 - End of Life Software"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# VUL.3.02 - End of Life Software

## Control Statement

Identified end-of-life software must have a documented decommission plan in place before the software is removed from the environment.

## Context

All software has an end-of-life timeframe that must be taken into consideration.  By documenting a decommission plan we take into account all of the ways sofware used connects to and indirectly connects to other pieces of software, systems, etc. along with custom scripts, automation, etc. that might rely on the software and can become detrimental to business operations.  By creating a decommission plan we reduce the risk of interrupting the business when software is removed from service.

## Scope

This control applies to all software utilized within our production environment that supports business operations. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

* Control Owners: 
  * Business Operations
* Process owner(s):
    * Infrastructure
    * Finance
    * Business Operations
    * IT Operations


## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [End of Life Software control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/-/issues/1718).

Examples of evidence an auditor might request to satisfy this control:

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC6.5